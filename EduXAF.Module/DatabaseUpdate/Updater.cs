using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using EduXAF.Module.BusinessObjects.HR;

namespace EduXAF.Module.DatabaseUpdate
{
    public class Updater : ModuleUpdater
    {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) : base(objectSpace, currentDBVersion) { }
        public override void UpdateDatabaseAfterUpdateSchema()
        {
            base.UpdateDatabaseAfterUpdateSchema();
            CreateUser();
        }

        public void CreateUser()
        {
            SecuritySystemRole adminRole = ObjectSpace.FindObject<SecuritySystemRole>(CriteriaOperator.Parse("Name=?", "admin"));
            if (adminRole == null)
            {
                adminRole = ObjectSpace.CreateObject<SecuritySystemRole>();
                adminRole.Name = "admin";
                adminRole.IsAdministrative = true;
            }
            adminRole.Save();

            EduUser user = ObjectSpace.FindObject<EduUser>(CriteriaOperator.Parse("UserName=?", "Admin"));
            if (user == null)
            {
                user = ObjectSpace.CreateObject<EduUser>();
                user.UserName = "Admin";
            }
            user.Roles.Add(adminRole);
            user.Save();
        }
    }
}
