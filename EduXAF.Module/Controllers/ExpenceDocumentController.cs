﻿ 
    using DevExpress.ExpressApp;
    using DevExpress.XtraEditors;
    using EduXAF.Module.BusinessObjects;
    using EduXAF.Module.BusinessObjects.CashCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    namespace EduXAF.Module.Controllers
    {
        public partial class ExpenceDocumentController : ViewController
        {
            Cash OldCash;

            public ExpenceDocumentController()
            {
                InitializeComponent();
                RegisterActions(components);
                // For instance, you can specify activation conditions of a Controller or create its Actions (http://documentation.devexpress.com/#Xaf/CustomDocument2622).
                TargetObjectType = typeof(ExpenseDocument);
                TargetViewType = ViewType.DetailView;

            }
            protected override void OnActivated()
            {
                base.OnActivated();
                ObjectSpace.ObjectSaved += ObjectSpace_ObjectSaved;
                ObjectSpace.Committing += ObjectSpace_Committing;
                if (View.CurrentObject != null && View.CurrentObject.GetType() == typeof(ExpenseDocument) && !ObjectSpace.IsNewObject(View.CurrentObject))
                {
                    OldCash = (View.CurrentObject as ExpenseDocument).Cash;
                }
            }
            int CommitCount = 0;
            void ObjectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
            {
                if (View.CurrentObject.GetType() == typeof(ExpenseDocument) && CommitCount == 0)
                {
                    ExpenseDocument cmd = View.CurrentObject as ExpenseDocument;
                    decimal cashAmount = CalculateCashAmount(cmd);
                    if (cashAmount < cmd.TotalAmount)
                    {
                        e.Cancel = true;
                        XtraMessageBox.Show(String.Format("Əməliyyat baş tutmadı. Kassada cəmi {0} məbləğ vardır. Əməliyyatın davam etdirilməsi üçün {1} qədər məbləğ çatmır.", cashAmount.ToString("C2"), (cashAmount - (decimal)cmd.TotalAmount).ToString("C2")));
                    }
                }
            }

            public decimal CalculateCashAmount(ExpenseDocument cmd)
            {
                DateTime date = cmd.InputDate;
                var data = from h in cmd.Cash.CashHistories
                           where h.InputDate <= date && h.ResourceID != cmd.Oid
                           select h;
                return data.Sum(h => h.Amount);

            }
            void ObjectSpace_ObjectSaved(object sender, ObjectManipulatingEventArgs e)
            {
                if (e.Object.GetType() == typeof(ExpenseDocument))
                {
                    ExpenseDocument NewDocument = e.Object as ExpenseDocument;
                    if (OldCash != null)
                        DoDelete(OldCash, NewDocument.Oid);

                    DoSave(NewDocument);
                }
            }

            void DoSave(ExpenseDocument cid)
            {

                //Add history for Cash        
                CashHistory tempHistory = View.ObjectSpace.CreateObject<CashHistory>();
                tempHistory.Type = CashHistoryType.CashExpence;
                tempHistory.RefferencedInvoiceNumber = cid.InvoiceNumber;
                tempHistory.ResourceID = cid.Oid;
                tempHistory.Amount = Convert.ToDecimal(cid.TotalAmount) * (-1);
                tempHistory.InputDate = cid.InputDate;
                tempHistory.Status = String.Format("cash expence document Item New added - {0}", cid.Oid);

                Cash cash = ObjectSpace.GetObjectByKey<Cash>(cid.Cash.Oid);
                cash.CashHistories.Add(tempHistory);
                cash.Save();
                OldCash = cash;

                CommitCount += 1;
                ObjectSpace.CommitChanges();

            }

            void DoDelete(Cash Oldcash, Guid Did)
            {
                //Delete Cash History
                Cash cash = ObjectSpace.GetObjectByKey<Cash>(Oldcash.Oid);
                IEnumerable<CashHistory> CashCollection = cash.CashHistories.Where(h => h.ResourceID == Did);
                foreach (CashHistory history in CashCollection.ToList())
                {
                    ObjectSpace.GetObjectByKey<CashHistory>(history.Oid).Delete();
                }
                cash.Save();

            }
        }
    }
 
