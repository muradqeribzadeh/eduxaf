namespace EduXAF.Module.Controllers
{
    partial class CardPass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PassCard = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            // 
            // PassCard
            // 
            this.PassCard.Caption = "Pass Card";
            this.PassCard.ConfirmationMessage = null;
            this.PassCard.Id = "PassCard";
            this.PassCard.ImageName = null;
            this.PassCard.NullValuePrompt = null;
            this.PassCard.ShortCaption = null;
            this.PassCard.Shortcut = null;
            this.PassCard.Tag = null;
            this.PassCard.TargetObjectsCriteria = null;
            this.PassCard.TargetViewId = null;
            this.PassCard.ToolTip = null;
            this.PassCard.TypeOfView = null;
            this.PassCard.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.PassCard_Execute);
            // 
            // CardPass
            // 
            this.TargetObjectType = typeof(EduXAF.Module.BusinessObjects.Products.Course.Lesson);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.ParametrizedAction PassCard;
    }
}
