using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EduXAF.Module.BusinessObjects.Products.Course;

namespace EduXAF.Module.Controllers
{
    public partial class ChangeStudentDetailStatus : ViewController
    {
        public ChangeStudentDetailStatus()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        

        private void present_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            LessonStudentDetail student = (LessonStudentDetail)e.CurrentObject;
            student.Status = BusinessObjects.LessonStudentStatus.present;
            student.Save();            
        }

        private void None_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            LessonStudentDetail student = (LessonStudentDetail)e.CurrentObject;
            student.Status = BusinessObjects.LessonStudentStatus.none;
            student.Save();  
        }

        private void Absent_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            LessonStudentDetail student = (LessonStudentDetail)e.CurrentObject;
            student.Status = BusinessObjects.LessonStudentStatus.absent;
            student.Save();  
        }

        private void ForceMajor_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            LessonStudentDetail student = (LessonStudentDetail)e.CurrentObject;
            student.Status = BusinessObjects.LessonStudentStatus.forcemajor;
            student.Save();  
        }

        private void PermissionedAbsent_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            LessonStudentDetail student = (LessonStudentDetail)e.CurrentObject;
            student.Status = BusinessObjects.LessonStudentStatus.permissionedabsent;
            student.Save();  
        }
    }
}
