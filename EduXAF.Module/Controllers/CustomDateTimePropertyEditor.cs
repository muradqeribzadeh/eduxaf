﻿using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win.Editors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduXAF.Module.Controllers
{
    [PropertyEditor(typeof(DateTime), false)]
    public class CustomDateTimeEditor : DatePropertyEditor
    {
        public CustomDateTimeEditor(
            Type objectType, IModelMemberViewItem info)
            : base(objectType, info) { }
        protected override void SetupRepositoryItem(
            DevExpress.XtraEditors.Repository.RepositoryItem item)
        {
            base.SetupRepositoryItem(item);
            ((RepositoryItemDateTimeEdit)item).VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            ((RepositoryItemDateTimeEdit)item).VistaEditTime = DevExpress.Utils.DefaultBoolean.True;
            ((RepositoryItemDateTimeEdit)item).Mask.EditMask = "dd.MM.yyyy hh:mm:ss";
        }
    }
}
