using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.XtraScheduler;
using DevExpress.ExpressApp.Scheduler.Win;
using EduXAF.Module.BusinessObjects.Products.Course;
using DevExpress.Data.Filtering;
using EduXAF.Module.BusinessObjects;
using System.Drawing;

namespace EduXAF.Module.Controllers
{
    public partial class ColorizeStudentActivityController : ViewController
    {
        public ColorizeStudentActivityController()
        {
            InitializeComponent();
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            View.ControlsCreated += View_ControlsCreated;
        }

        public SchedulerControl scheduler = null;
        void View_ControlsCreated(object sender, EventArgs e)
        {
            //Do some with Scheduler Control 
            if (View is ListView)
            {
                SchedulerListEditor editor = ((ListView)View).Editor as SchedulerListEditor;
                if (editor == null || editor.SchedulerControl == null) return;
                scheduler = editor.SchedulerControl;
                if (scheduler != null)
                {
                    scheduler.AppointmentViewInfoCustomizing += scheduler_AppointmentViewInfoCustomizing;

                }
            }
        }
        void scheduler_AppointmentViewInfoCustomizing(object sender, AppointmentViewInfoCustomizingEventArgs e)
        {
            Appointment apt = e.ViewInfo.Appointment;
            object lsnID;
            if (apt.Id == null)
                lsnID = apt.RecurrencePattern.Id;
            else
                lsnID = apt.Id;

            Lesson lsn =
                View.ObjectSpace.GetObjectByKey<Lesson>(lsnID);


            lsn.StudentDetails.CriteriaString = "";
            lsn.StudentDetails.Criteria = CriteriaOperator.Parse("Student.Oid = ?", e.ViewInfo.Resource.Id);
            LessonStudentDetail lsd = lsn.StudentDetails[0];

            switch (lsd.Status)
            {
                case LessonStudentStatus.none:
                    break;
                case LessonStudentStatus.absent:
                    e.ViewInfo.Appearance.BackColor = Color.Red;
                    break;
                case LessonStudentStatus.present:
                    e.ViewInfo.Appearance.BackColor = Color.SpringGreen;
                    break;
                case LessonStudentStatus.permissionedabsent:
                    e.ViewInfo.Appearance.BackColor = Color.SkyBlue;
                    break;
                case LessonStudentStatus.forcemajor:
                    e.ViewInfo.Appearance.BackColor = Color.Gray;
                    break;
                default:
                    break;
            }

        }
    }
}
