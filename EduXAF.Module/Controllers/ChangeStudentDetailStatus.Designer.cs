namespace EduXAF.Module.Controllers
{
    partial class ChangeStudentDetailStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Present = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Absent = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ForceMajor = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PermissionedAbsent = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.None = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // Present
            // 
            this.Present.Caption = "Do Present";
            this.Present.ConfirmationMessage = null;
            this.Present.Id = "doPresent";
            this.Present.ImageName = null;
            this.Present.Shortcut = null;
            this.Present.Tag = null;
            this.Present.TargetObjectsCriteria = null;
            this.Present.TargetViewId = null;
            this.Present.ToolTip = null;
            this.Present.TypeOfView = null;
            this.Present.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.present_Execute);
            // 
            // Absent
            // 
            this.Absent.Caption = "Do Adbsent";
            this.Absent.ConfirmationMessage = null;
            this.Absent.Id = "Adbsent";
            this.Absent.ImageName = null;
            this.Absent.Shortcut = null;
            this.Absent.Tag = null;
            this.Absent.TargetObjectsCriteria = null;
            this.Absent.TargetViewId = null;
            this.Absent.ToolTip = null;
            this.Absent.TypeOfView = null;
            this.Absent.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Absent_Execute);
            // 
            // ForceMajor
            // 
            this.ForceMajor.Caption = "Do Force Major";
            this.ForceMajor.ConfirmationMessage = null;
            this.ForceMajor.Id = "ForceMajor";
            this.ForceMajor.ImageName = null;
            this.ForceMajor.Shortcut = null;
            this.ForceMajor.Tag = null;
            this.ForceMajor.TargetObjectsCriteria = null;
            this.ForceMajor.TargetViewId = null;
            this.ForceMajor.ToolTip = null;
            this.ForceMajor.TypeOfView = null;
            this.ForceMajor.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ForceMajor_Execute);
            // 
            // PermissionedAbsent
            // 
            this.PermissionedAbsent.Caption = "Permissioned Absent";
            this.PermissionedAbsent.ConfirmationMessage = null;
            this.PermissionedAbsent.Id = "permissionedabsent";
            this.PermissionedAbsent.ImageName = null;
            this.PermissionedAbsent.Shortcut = null;
            this.PermissionedAbsent.Tag = null;
            this.PermissionedAbsent.TargetObjectsCriteria = null;
            this.PermissionedAbsent.TargetViewId = null;
            this.PermissionedAbsent.ToolTip = null;
            this.PermissionedAbsent.TypeOfView = null;
            this.PermissionedAbsent.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PermissionedAbsent_Execute);
            // 
            // None
            // 
            this.None.Caption = "None";
            this.None.ConfirmationMessage = null;
            this.None.Id = "None";
            this.None.ImageName = null;
            this.None.Shortcut = null;
            this.None.Tag = null;
            this.None.TargetObjectsCriteria = null;
            this.None.TargetViewId = null;
            this.None.ToolTip = null;
            this.None.TypeOfView = null;
            this.None.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.None_Execute);
            // 
            // ChangeStudentDetailStatus
            // 
            this.TargetObjectType = typeof(EduXAF.Module.BusinessObjects.Products.Course.LessonStudentDetail);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Nested;
            this.TypeOfView = typeof(DevExpress.ExpressApp.View);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction Present;
        private DevExpress.ExpressApp.Actions.SimpleAction Absent;
        private DevExpress.ExpressApp.Actions.SimpleAction ForceMajor;
        private DevExpress.ExpressApp.Actions.SimpleAction PermissionedAbsent;
        private DevExpress.ExpressApp.Actions.SimpleAction None;
    }
}
