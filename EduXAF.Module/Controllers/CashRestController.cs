using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using EduXAF.Module.BusinessObjects.CashCore;
using EduXAF.Module.BusinessObjects;

namespace EduXAF.Module.Controllers
{ 
       public partial class CashRestController : ViewController
    {
        public Cash OldCash;
        public CashRestController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(CashRestDocument);
            TargetViewType = ViewType.DetailView;
            
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            ObjectSpace.ObjectSaved += ObjectSpace_ObjectSaved;

            if (View.CurrentObject != null && View.CurrentObject.GetType() == typeof(CashRestDocument) && !ObjectSpace.IsNewObject(View.CurrentObject))
            {
                OldCash = (View.CurrentObject as CashRestDocument).Cash;
            }
        }

        void ObjectSpace_ObjectSaved(object sender, ObjectManipulatingEventArgs e)
        {
            if (e.Object.GetType() == typeof(CashRestDocument))
            {
                CashRestDocument NewDocument = e.Object as CashRestDocument;
                if (OldCash != null)
                    DoDelete(OldCash, NewDocument.Oid);

                DoSave(NewDocument);
            }
        }

        void DoSave(CashRestDocument cid)
        {

            //Add history for Cash                    
            CashHistory tempHistory = View.ObjectSpace.CreateObject<CashHistory>();
            tempHistory.Type = CashHistoryType.CashRest;
            tempHistory.RefferencedInvoiceNumber = cid.InvoiceNumber;
            tempHistory.ResourceID = cid.Oid;
            tempHistory.Amount = cid.Amount;
            tempHistory.InputDate = cid.InputDate;
            tempHistory.Status = String.Format("cash rest Item New added - {0}", cid.Oid);

            Cash cash = ObjectSpace.GetObjectByKey<Cash>(cid.Cash.Oid);
            cash.CashHistories.Add(tempHistory);
            cash.Save();
            OldCash = cash;

            ObjectSpace.CommitChanges();

        }

        void DoDelete(Cash Oldcash, Guid Did)
        {
            //Delete Cash History
            Cash cash = ObjectSpace.GetObjectByKey<Cash>(Oldcash.Oid);
            IEnumerable<CashHistory> CashCollection = cash.CashHistories.Where(h => h.ResourceID == Did);
            foreach (CashHistory history in CashCollection.ToList())
            {
                ObjectSpace.GetObjectByKey<CashHistory>(history.Oid).Delete();
            }
            cash.Save();
            
        }

    }
}
