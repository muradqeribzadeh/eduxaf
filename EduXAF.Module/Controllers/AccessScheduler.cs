using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using EduXAF.Module.BusinessObjects.Products.Course;
using DevExpress.ExpressApp.Scheduler.Win;
using DevExpress.XtraScheduler;
using DrawingAlias = System.Drawing;
using DevExpress.Data.Filtering;
using EduXAF.Module.BusinessObjects;
using DevExpress.XtraScheduler.UI;

namespace EduXAF.Module.Controllers
{
    public partial class AccessScheduler : ViewController
    {
        public AccessScheduler()
        {
            InitializeComponent();
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            if (View.ObjectTypeInfo.Implements<Lesson>())
                View.ControlsCreated += View_ControlsCreated;
        }
        public SchedulerControl scheduler = null;
        void View_ControlsCreated(object sender, EventArgs e)
        {

            //Do some with Scheduler Control 
            if (View is ListView)
            {
                SchedulerListEditor editor = ((ListView)View).Editor as SchedulerListEditor;
                if (editor == null || editor.SchedulerControl == null) return;
                SchedulerControl scheduler = editor.SchedulerControl;
                if (scheduler != null)
                {
                    scheduler.PopupMenuShowing += scheduler_PopupMenuShowing;
                    SetupLabels(scheduler.Storage);
                }
            }
            else if (View is DetailView)
            {
                foreach (SchedulerLabelPropertyEditor pe in ((DetailView)View).GetItems<SchedulerLabelPropertyEditor>())
                {
                    if (pe.Control != null)
                    {
                        SetupLabels(((AppointmentLabelEdit)pe.Control).Storage);
                    }
                    else
                    {
                        //  pe.ControlCreated += new EventHandler<EventArgs>(pe_ControlCreated);
                    }
                }
            }
        }
        void scheduler_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {

            e.Menu.RemoveMenuItem(SchedulerMenuItemId.NewAllDayEvent);
            e.Menu.RemoveMenuItem(SchedulerMenuItemId.StatusSubMenu);
            // e.Menu.Items[3].Visible = false;
        }
        private static void SetupLabels(SchedulerStorage storage)
        {
            storage.Appointments.Labels.Clear();
            storage.Appointments.Statuses.Clear();
        }

       
    }
}
