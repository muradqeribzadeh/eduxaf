﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduXAF.Module.BusinessObjects
{
    class Enums
    {
    }

    public enum RelativeType
    {
        Father,
        Mother,
        Brother,
        Sister,
        Spouse,
        Friend
    };

    public enum TitleOfCourtesy
    {
        Dr,
        Miss,
        Mr,
        Mrs,
        Ms
    };

    public enum Gender
    {
        Male,
        Female
    }

    public enum PhoneTypes
    {
        Mobile, 
        Home, 
        Work,
    }

    public enum WebContactTypes
    {
        Facebook,
        Skype,
        Twitter,
        GTalk,
    }

    public enum LessonStudentStatus
    {
        none = 0,
        present = 1,
        absent = 2,
        permissionedabsent = 3,
        forcemajor = 4
    }

    public enum PaymentFace
    {
        Company = 0, //Yani GULF ozu odeyir
        Student = 1, //Telebe ozu odeyecek
        Sponsor = 2, //Telebenin sponsoru odeyecek
        Goverment = 3 //Dovlet verecek pulunu
    }
    public enum CashHistoryType
    {
        FromStudent = 1,
        CashExpence = 2,
        CashRest = 3,
        ToTeacher = 4

    }
}
