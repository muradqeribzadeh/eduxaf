using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace EduXAF.Module.BusinessObjects.CRM
{
    [DefaultClassOptions]
    public class WebContact : BaseObject
    {
        #region Constructors
        public WebContact(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        #endregion

        #region Properties
        private string _UserID;
        private bool _LikeContact;
        private WebContactTypes _ContactType;

        public bool LikeContact
        {
            get
            {
                return _LikeContact;
            }
            set
            {
                SetPropertyValue("LikeContact", ref _LikeContact, value);
            }
        }

        public WebContactTypes ContactType
        {
            get { return _ContactType; }
            set { SetPropertyValue("ContactType", ref _ContactType, value); }
        }

        public string UserID
        {
            get
            {
                return _UserID;
            }
            set
            {
                SetPropertyValue("UserID", ref _UserID, value);
            }
        }


        private CustomerBase _Customer;
        [VisibleInDetailView(false)]
        [Association("CustomerBase-WebContacts")]
        public CustomerBase Customer
        {
            get
            {
                return _Customer;
            }
            set
            {
                SetPropertyValue<CustomerBase>("Customer", ref _Customer, value);
            }
        }
        #endregion

    }

}
