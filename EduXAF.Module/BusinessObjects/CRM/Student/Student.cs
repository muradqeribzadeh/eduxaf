using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Products.Course;
using EduXAF.Module.BusinessObjects.Payment;
using DevExpress.Persistent.Base.General;

namespace EduXAF.Module.BusinessObjects.CRM.Student
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [ImageName("BO_Employee"), System.ComponentModel.DisplayName("Student")]
    public class Student : CustomerBase, IResource
    {
        #region Construcors
        public Student(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.

            _color = Color.LightYellow.ToArgb();
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        #endregion

        #region Properties

        

        //[Association("Student-Invoices", typeof(StudentInvoice))]
        //public XPCollection<StudentInvoice> Invoices
        //{
        //    get { return GetCollection<StudentInvoice>("Invoices"); }
        //}

        [Association("Student-LessonDetails", typeof(LessonStudentDetail))]
        public XPCollection<LessonStudentDetail> LessonDetails
        {
            get { return GetCollection<LessonStudentDetail>("LessonDetails"); }
        }

        private Lesson _activity;
        [Association("Lesson-Students",  typeof(Lesson))]
        public Lesson Activity
        {
            get{
                return _activity;
            }
            set{
                SetPropertyValue<Lesson>("Activity", ref _activity, value);
            }
        }

        [Association("Students-StudentInvoice", typeof(StudentInvoice)), Aggregated]
        public XPCollection<StudentInvoice> StudentInvoices
        {
            get { return GetCollection<StudentInvoice>("StudentInvoices"); }
        }
      
        #endregion
        private string passportNumber;
        public string PassportNumber
        {
            get { return passportNumber; }
            set { passportNumber = value; }
        }

        private PaymentFace _Payer;
        public PaymentFace Payer
        {
            get
            {
                return _Payer;
            }
            set
            {
                SetPropertyValue("Payer", ref _Payer, value);
            }
        }
        private string _Description;
        [Size(1024)]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                SetPropertyValue("Description", ref _Description, value);
            }
        }
        private string _CardNumber;
        public string CardNumber
        {
            get
            {
                return _CardNumber;
            }
            set
            {
                SetPropertyValue("CardNumber", ref _CardNumber, value);
            }
        }

        #region IResource Members

        [Persistent("Color")]
        private Int32 _color;

        [NonPersistent, Browsable(false)]
        public object Id
        {
            get { return Oid; }
        }


        public string Caption
        {
            get { return FullName; }
            set
            {
               // SetPropertyValue("Caption", ref FullName, value);
            }
        }

        [NonPersistent, Browsable(false)]
        public Int32 OleColor
        {
            get { return ColorTranslator.ToOle(Color.FromArgb(_color)); }
        }

        //[Association("Student-ClassDetails", typeof(ClassStudentDetail))]
        //public XPCollection<ClassStudentDetail> ClassDetails
        //{
        //    get { return GetCollection<ClassStudentDetail>("ClassDetails"); }
        //}

        [NonPersistent]
        public Color Color
        {
            get { return Color.FromArgb(_color); }
            set
            {
                _color = value.ToArgb();
                OnChanged("Color");
            }
        }

        #endregion IResource Members
    }

}
