using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EduXAF.Module.BusinessObjects.BaseClass;

namespace EduXAF.Module.BusinessObjects.CRM
{
    [DefaultClassOptions]
    public class CustomerBase : PersonBase
    {
        #region Constructions
        public CustomerBase(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        #endregion

        #region Associations
        //[Association("CustomerBase-Relative", typeof(CustomerRelative)), Aggregated]
        //public XPCollection<CustomerRelative> Relatives
        //{
        //    get
        //    {
        //        return GetCollection<CustomerRelative>("Relatives");
        //    }
        //}

        [Association("CustomerBase-WebContacts", typeof(WebContact)), Aggregated]
        public XPCollection<WebContact> WebContacts
        {
            get
            {
                return GetCollection<WebContact>("WebContacts");
            }
        }

        [Association("CustomerBase-Sponsor", typeof(CustomerSponsor)), Aggregated]
        public XPCollection<CustomerSponsor> Sponsors
        {
            get
            {
                return GetCollection<CustomerSponsor>("Sponsors");
            }
        }
        #endregion

        #region Properties
        
        #endregion
    }

    
}
