using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EduXAF.Module.BusinessObjects.BaseClass;

namespace EduXAF.Module.BusinessObjects.CRM
{
    [DefaultClassOptions]
    public class PhoneContact : BaseObject
    {
        #region Constructors
        public PhoneContact(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        #endregion

        #region Properties
        private PhoneTypes _PhoneType;
        private string _Number;
        private PersonBase _Person;
        private CustomerSponsor _Sponsor;
        public string Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        public PhoneTypes PhoneType
        {
            get { return _PhoneType; }
            set { SetPropertyValue("PhoneType", ref _PhoneType, value); }
        }

        [VisibleInDetailView(false)]
        [Association("PersonBase-PhoneContact")]
        public PersonBase Person
        {            
            get { return _Person; }
            set { SetPropertyValue("PhoneType", ref _Person, value); }
        }


        [VisibleInDetailView(false)]
        [Association("CustomerSponsor-PhoneContact")]
        public CustomerSponsor Sponsor
        {
            get { return _Sponsor; }
            set { SetPropertyValue("Sponsor", ref _Sponsor, value); }
        }
        #endregion
    }

}
