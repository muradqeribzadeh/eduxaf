using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace EduXAF.Module.BusinessObjects.CRM
{
    [DefaultClassOptions]
    public class CustomerSponsor : BaseObject
    {
        #region Constructors
        public CustomerSponsor(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        #endregion

        private string _Email;
        private Address _Address;
        private string _ContactPerson;
        private string _firstname = String.Empty;
        private string _lastname = String.Empty;
        private string _Organization = String.Empty;
        
       


        [Size(600)]
        public string Organization
        {
            get { return _Organization; }
            set { SetPropertyValue("Organization", ref _Organization, value); }
        }

        public string ContactPerson
        {
            get { return _ContactPerson; }
            set { SetPropertyValue("ContactPerson", ref _ContactPerson, value); }
        }

        public string FirstName
        {
            get { return _firstname; }
            set { SetPropertyValue("FirstName", ref _firstname, value); }
        }


        public string LastName
        {
            get { return _lastname; }
            set { SetPropertyValue("LastName", ref _lastname, value); }
        }

        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                SetPropertyValue("Email", ref _Email, value);
            }
        }

        [Aggregated]
        [ExpandObjectMembers(ExpandObjectMembers.Never)]
        public Address Address
        {
            get
            {
                return _Address;
            }
            set
            {
                SetPropertyValue("Address", ref _Address, value);
            }
        }


        private CustomerBase _Customer;
        [VisibleInDetailView(false)]
        [Association("CustomerBase-Sponsor")]
        public CustomerBase Customer
        {
            get
            {
                return _Customer;
            }
            set
            {
                SetPropertyValue<CustomerBase>("Customer", ref _Customer, value);
            }
        }


        #region Associations

        [Association("CustomerSponsor-PhoneContact", typeof(PhoneContact)), Aggregated]
        public XPCollection<PhoneContact> PhoneContacts
        {
            get
            {
                return GetCollection<PhoneContact>("PhoneContacts");
            }
        }


        #endregion
    }

}
