using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Products.Course;
using EduXAF.Module.BusinessObjects.Payment;
using DevExpress.Persistent.Base.General;

namespace EduXAF.Module.BusinessObjects.Payment
{
    public class InvoiceItem : BaseObject
    {
        public InvoiceItem(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }

        //private int _count;
        //public int Count
        //{
        //    get { return _count; }
        //    set { SetPropertyValue<int>("Count", ref _count, value); }
        //}

        private decimal _amount;

        [ImmediatePostData]
        public decimal Amount
        {
            get { return _amount; }
            set
            {
                SetPropertyValue<decimal>("Amount", ref _amount, value);
                if (!IsLoading && !IsSaving && Invoice != null)
                {
                    Invoice.UpdateItemsTotal(true);
                   
                }
            }
        }

        private Invoice _invoice;

        [Association("Invoice-InvoiceItems", typeof(Invoice))]
        public Invoice Invoice
        {
            get { return _invoice; }
            set {
                Invoice oldInvoice = _invoice;
                SetPropertyValue<Invoice>("Invoice", ref _invoice, value);
                if (!IsLoading && !IsSaving && oldInvoice != _invoice)
                {
                    oldInvoice = oldInvoice ?? _invoice;
                    oldInvoice.UpdateItemsTotal(true);
                }
            }
        }
    }
}