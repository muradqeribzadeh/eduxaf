﻿using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Products.Course;
using EduXAF.Module.BusinessObjects.Payment;
using DevExpress.Persistent.Base.General;
using EduXAF.Module.BusinessObjects.CRM.Student;

namespace EduXAF.Module.BusinessObjects.Payment
{
    [DefaultClassOptions]
    [DefaultProperty("Student")]
    public class StudentInvoice : Invoice
    {
        public StudentInvoice(Session session)
            : base(session)
        {
        }

        private Group _Group;
        private InvoiceItem _invoiceItem;

        //[NonPersistent]
       // [ExpandObjectMembers(ExpandObjectMembers.Always)]
        //public InvoiceItem InvoiceItem
        //{
        //    get { return InvoiceItems[0]; }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //_invoiceItem = new InvoiceItem(Session)
            //{
            //    Name = String.Format("For {0} Lessons", Group.Course.Name),
            //    Amount = Group.Course.Cost
            //};
            //InvoiceItems.Add(_invoiceItem);
        }

       
        //[Association("Student-Invoices", typeof(Student))]
        //public Student Student
        //{
        //    get { return _student; }
        //    set
        //    {
        //        SetPropertyValue<Student>("Student", ref _student, value);
        //        AutocompletInvoiceItemName();
        //    }
        //}

      

        private Student _student;
        [Association("Students-StudentInvoice", typeof(Student))]
        public Student Student
        {
            get { return _student; }
            set
            {
                SetPropertyValue<Student>("Student", ref _student, value);
               // AutocompletInvoiceItemName();
                
            }
        }

        public Group Group
        {
            get
            {
                return _Group;
            }
            set
            {
                SetPropertyValue("Group", ref _Group, value);
            }
        }

        //public void AutocompletInvoiceItemName()
        //{
        //    if (!IsLoading && InvoiceItem != null)
        //    {
        //        if (_Group != null)
        //        {
        //            InvoiceItem.Amount = _Group.Course.Cost;
        //            InvoiceItem.Name = _Group.Course.Name;
        //        }
        //        if (_student != null)
        //        {
        //            InvoiceItem.Name += " - " + _student.FullName;
        //        }
        //    }
        //}
    }
}