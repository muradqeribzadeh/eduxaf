﻿using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Products.Course;
using EduXAF.Module.BusinessObjects.Payment;
using DevExpress.Persistent.Base.General;
using EduXAF.Module.BusinessObjects.HR.Tutor;

namespace EduXAF.Module.BusinessObjects.Payment
{
    [DefaultClassOptions]
    [DefaultProperty("InvoiceNumber")]
    public class TutorInvoice : Invoice
    {
        public TutorInvoice(Session session) : base(session)
        {
        }


        private Tutor _tutor;
        [Association("Tutor-Invoices", typeof (Tutor))]
        public Tutor Tutor
        {
            get { return _tutor; }
            set { SetPropertyValue<Tutor>("Tutor", ref _tutor, value); }
        }

        //private Class _class;
        //public Class Classes
        //{
        //    get { return _class; }
        //    set
        //    {
        //        _class = value;
        //        OnChanged("Classes");
        //    }
        //}

        //[Association("TutorInvoice-Classes", typeof(Class))]
        //public XPCollection<Class> Classes
        //{
        //    get { return GetCollection<Class>("Classes"); }
        //}

    }
}