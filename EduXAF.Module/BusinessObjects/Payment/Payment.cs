using System;
using System.ComponentModel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Products.Course;

using DevExpress.Persistent.Base.General;
using EduXAF.Module.BusinessObjects.CashCore;

namespace EduXAF.Module.BusinessObjects.Payment
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class Payment : BaseObject
    {
        public Payment(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            PaymentDate = DateTime.Now;
        }
        private Cash _Cash;
        private string _name;
    
        [RuleRequiredField(DefaultContexts.Save)]
        public Cash Cash
        {
            get
            {
                return _Cash;
            }
            set
            {
                SetPropertyValue("Cash", ref _Cash, value);
            }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }        
        
        private DateTime _paymentDate;
        public DateTime PaymentDate
        {
            get { return _paymentDate; }
            set { SetPropertyValue<DateTime>("PaymentDate", ref _paymentDate, value); }
        }

        private decimal _amountPaid;
        public decimal AmountPaid
        {
            get { return _amountPaid; }
            set {
                SetPropertyValue<decimal>("AmountPaid", ref _amountPaid, value);
                if (!IsLoading && !IsSaving && Invoice != null)
                {
                    Invoice.UpdatePaymentTotal(true);
                }
            }
        }

        private Invoice _invoice;
        [Association("Invoice-Payments", typeof (Invoice))]
        public Invoice Invoice
        {
            get { return _invoice; }
            set {
                Invoice oldInvoice = _invoice;
                SetPropertyValue<Invoice>("Invoice", ref _invoice, value);
                if (!IsLoading && !IsSaving && oldInvoice != _invoice)
                {
                    oldInvoice = oldInvoice ?? _invoice;
                    oldInvoice.UpdatePaymentTotal(true);
                }
            }
        }
    }
}