using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Products.Course;
using EduXAF.Module.BusinessObjects.Payment;
using DevExpress.Persistent.Base.General;

namespace EduXAF.Module.BusinessObjects.Payment
{
    [FriendlyKeyProperty("InvoiceNumber")]
    public class Invoice : XPBaseObject
    {
        public Invoice(Session session)
            : base(session)
        {
            
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            InvoiceDate = DateTime.Now;
            InvoiceNumber = "INV-" + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);
        }

        protected override void OnLoaded()
        {
            fTotalPaid = null;
            _totalCost = null;
            base.OnLoaded();
            
           // InvoiceItems.CollectionChanged += new XPCollectionChangedEventHandler(InvoiceItems_CollectionChanged);
        }

        //private void InvoiceItems_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        //{
        //    RecalcTotalCost();
        //}

        [Association("Invoice-InvoiceItems", typeof(InvoiceItem)), Aggregated]
        public XPCollection<InvoiceItem> InvoiceItems
        {
            get { return GetCollection<InvoiceItem>("InvoiceItems"); }
        }

        [Association("Invoice-Payments", typeof(Payment)), Aggregated]
        public XPCollection<Payment> Payments
        {
            get { return GetCollection<Payment>("Payments"); }
        }

        private string _invoiceNumber;
        [Key(false)]
        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { SetPropertyValue<string>("InvoiceNumber", ref _invoiceNumber, value); }
        }

       

        [Persistent]
        public decimal? AmountDue
        {
            get { return TotalCost - TotalPaid; }
        }

        [Persistent]
        public bool IsPaidInFull
        {
            get { return AmountDue <= 0; }
        }

        [Persistent("TotalCost")]
        private decimal? _totalCost=null;
        [PersistentAlias("_totalCost")]
      
        public decimal? TotalCost
        {
            get {
                if (!IsLoading && !IsSaving && _totalCost == null)
                    UpdateItemsTotal(false);
                return _totalCost; 
            }
            
        }
        
   
        [Persistent("TotalPaid")]
        private decimal? fTotalPaid = null;
        [PersistentAlias("fTotalPaid")]
       
        public decimal? TotalPaid
        {
            get
            {
                if (!IsLoading && !IsSaving && fTotalPaid == null)
                    UpdatePaymentTotal(false);
                return fTotalPaid;
            }
        }



        public void UpdateItemsTotal(bool forceChangeEvents)
        {
            //Put your complex business logic here. Just for demo purposes, we calculate a sum here.
            decimal? oldOrdersTotal = _totalCost;
            decimal tempTotal = 0m;
            //Manually iterate through the Orders collection if your calculated property requires a complex business logic which cannot be expressed via criteria language.
            foreach (InvoiceItem detail in InvoiceItems)
                tempTotal += detail.Amount;
            _totalCost = tempTotal;
            if (forceChangeEvents)
            {
               // OnChanged("AmountDue");
               
                OnChanged("TotalCost", oldOrdersTotal, _totalCost);
            }
        }
        public void UpdatePaymentTotal(bool forceChangeEvents)
        {
            //Put your complex business logic here. Just for demo purposes, we calculate a sum here.
            decimal? oldOrdersTotal = fTotalPaid;
            decimal tempTotal = 0m;
            //Manually iterate through the Orders collection if your calculated property requires a complex business logic which cannot be expressed via criteria language.
            foreach (Payment detail in Payments)
                tempTotal += detail.AmountPaid;
            fTotalPaid = tempTotal;
            if (forceChangeEvents)
                OnChanged("TotalPaid", oldOrdersTotal, fTotalPaid);
        }

        private DateTime _invoiceDate;

        public DateTime InvoiceDate
        {
            get
            {
                return _invoiceDate;
            }
            set
            {
                SetPropertyValue("InvoiceDate", ref _invoiceDate, value);
            }
        }
    }
}