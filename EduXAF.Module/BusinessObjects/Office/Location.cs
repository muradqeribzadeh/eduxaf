using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;

namespace EduXAF.Module.BusinessObjects.OfficeBase
{
    [DefaultClassOptions]
    [DefaultProperty("Title")]
    public class Location : BaseObject
    {
        public Location(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        #region Variables

        private Rectangle _PropertyName;
        private string name;
        private int capacity;

        [Persistent("Color")]
        private int _color;
        private Office office = null;

        #endregion Variables

        #region Properties

        public string Title
        {
            get
            {
                if (Office != null && Name != null)
                    return String.Format("{0} - {1}", Office.Name, Name);
                else
                    return "-";
            }
        }
        

        public Office Office
        {
            get
            {
                return office;
            }
            set
            {
                SetPropertyValue("Office", ref office, value);
            }
        }

        public string Name
        {
            get { return name; }
            set { SetPropertyValue<string>("Name", ref name, value); }
        }

        public int Capacity
        {
            get { return capacity; }
            set { SetPropertyValue<int>("Capacity", ref capacity, value); }
        }

        public object Id
        {
            get { return Oid; }
        }

        [NonPersistent, Browsable(false)]
        public string Caption
        {
            get { return name; }

        }

        public int OleColor
        {
            get { return ColorTranslator.ToOle(Color.FromArgb(_color)); }
        }

        [NonPersistent, Browsable(false)]
        public Color Color
        {
            get { return Color.FromArgb(_color); }
            set
            {
                _color = value.ToArgb();
                OnChanged("Color");
            }
        }

        #endregion Properties
    }

}
