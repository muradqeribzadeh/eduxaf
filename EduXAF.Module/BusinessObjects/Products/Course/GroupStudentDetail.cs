using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EduXAF.Module.BusinessObjects.CRM.Student;
using EduXAF.Module.BusinessObjects.Payment;

namespace EduXAF.Module.BusinessObjects.Products.Course
{
    [DefaultClassOptions]
    public class GroupStudentDetail : BaseObject
    {
        public GroupStudentDetail(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        private Student _student;
        public Student Student
        {
            get { return _student; }
            set
            {
                SetPropertyValue<Student>("Student", ref _student, value);
                
            }
        }

        private decimal _Amount;
        public decimal Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                SetPropertyValue("Amount", ref _Amount, value);
            }
        }
        private Group _Group;
        [Association("Group-StudentDetails", typeof(Group))]
        public Group Group
        {
            get { return _Group; }
            set
            {
                SetPropertyValue<Group>("Group", ref _Group, value);
               
            }
        }

       

    }

}
