using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Products.Course;

using DevExpress.Persistent.Base.General;
using EduXAF.Module.BusinessObjects.CRM.Student;

namespace EduXAF.Module.BusinessObjects.Products.Course
{
    public class LessonStudentDetail : BaseObject
    {
        public LessonStudentDetail(Session session)
            : base(session)
        {
        }

        private LessonStudentStatus _status;
        public LessonStudentStatus Status
        {
            get { return _status; }
            set { SetPropertyValue<LessonStudentStatus>("Status", ref _status, value); }
        }

        private Lesson _lesson;
        [Association("Lesson-StudentDetails", typeof(Lesson))]
        public Lesson Lesson
        {
            get { return _lesson; }
            set { SetPropertyValue<Lesson>("Lesson", ref _lesson, value); }
        }


        private Student _student;
        [Association("Student-LessonDetails", typeof(Student))]
        public Student Student
        {
            get { return _student; }
            set { SetPropertyValue<Student>("Student", ref _student, value); }
        }

    }
}