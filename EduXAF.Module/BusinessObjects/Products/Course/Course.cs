﻿using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Drawing;

using EduXAF.Module.BusinessObjects.Payment;
using DevExpress.Persistent.Base.General;

namespace EduXAF.Module.BusinessObjects.Products.Course
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class Course : BaseObject
    {
        private const int defaultLessonDuration = 70;
        private const int defaultLessonCount = 10;

        public Course(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            LessonCount = defaultLessonCount;
            LessonDuration = defaultLessonDuration;
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { SetPropertyValue<string>("Name", ref name, value); }
        }

        private string description;

        [Size(SizeAttribute.Unlimited), ObjectValidatorIgnoreIssue(typeof(ObjectValidatorLargeNonDelayedMember))]
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                OnChanged("Description");
            }
        }

        private int _lessonCount;

        public int LessonCount
        {
            get { return _lessonCount; }
            set
            {
                _lessonCount = value;
                OnChanged("LessonCount");
            }
        }

        private int _lessonDuration;

        public int LessonDuration
        {
            get { return _lessonDuration; }
            set { SetPropertyValue<int>("LessonDuration", ref _lessonDuration, value); }
        }

        private decimal _cost;

        public decimal Cost
        {
            get { return _cost; }
            set
            {
                _cost = value;
                OnChanged("Amount");
            }
        }
    }
}