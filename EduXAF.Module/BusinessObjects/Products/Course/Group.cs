using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win;
using DevExpress.Xpo;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Xml;
using DevExpress.XtraScheduler.UI;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using EduXAF.Module.BusinessObjects.HR.Tutor;
using EduXAF.Module.BusinessObjects.OfficeBase;
using EduXAF.Module.BusinessObjects.Payment;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace EduXAF.Module.BusinessObjects.Products.Course
{
    [DefaultClassOptions]
    [DefaultProperty("Caption")]
    public class Group : BaseObject
    {
        public Group(Session session)
            : base(session)
        {
            //StudentDetails.ListChanged += new ListChangedEventHandler(StudentDetails_ListChanged);
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Fields
        [NonPersistent]
        public string Caption
        {
            get { return (Course != null && StartOn != null && Location != null) ? this.Course.Name + " - " + this.StartOn.ToString("MM") + " - " + this.Location.Name : null; }
        }

        protected Course _course;
        public Course Course
        {
            get { return _course; }
            set
            {
                _course = value;
                OnChanged("Course");
            }
        }

        private DateTime _startOn;
        [ModelDefault("DisplayFormat", "{0:G}")]
        [ModelDefault("EditMask", "G")]
        [Custom("PropertyEditorType", "EduXAF.Module.Controllers.CustomDateTimeEditor")]
        [Indexed]
        public DateTime StartOn
        {
            get { return _startOn; }
            set
            {
                _startOn = value;
                OnChanged("StartOn");
            }
        }

        //private DateTime _endOn;
        //// [Custom("DisplayFormat", "{0: dd-MM-yyyy}")]
        //// [Custom("EditMask", "dd-MM-yyyy")]
        //[Custom("PropertyEditorType", "EduXAF.Module.Controllers.CustomDateTimeEditor")]
        //[Indexed]
        //public DateTime EndOn
        //{
        //    get { return _endOn; }
        //    set
        //    {
        //        _endOn = value;
        //        OnChanged("EndOn");
        //    }
        //}

        private string _description;
        [Size(SizeAttribute.Unlimited), ObjectValidatorIgnoreIssue(typeof(ObjectValidatorLargeNonDelayedMember))]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnChanged("Description");
            }
        }

        private Location _location;
        public Location Location
        {
            get { return _location; }
            set
            {
                _location = value;
                OnChanged("Location");
            }
        }
        private decimal _TutorFee;
        public decimal TutorFee
        {
            get
            {
                return _TutorFee;
            }
            set
            {
                SetPropertyValue("TutorFee", ref _TutorFee, value);
            }
        }
        #endregion
        


        //Actions.....
        bool isCreating = false;
        [Action]
        public void GenerateLessons()
        {
            if (StudentDetails.Count > 0)
            {
                try
                {
                    Lesson lsn;

                    if (1 == 2)//(Lessons.Count > 0)
                    {
                        
                        lsn = Lessons.First<Lesson>(i => i.Type == (int)AppointmentType.Pattern);
                        WinApplication.Messaging.Show("Information", "This Group have lesson yet!");
                    }
                    else
                    {
                        isCreating = true;
                        Appointment app = new Appointment(AppointmentType.Pattern);
                        app.RecurrenceInfo.Start = this.StartOn;
                        app.RecurrenceInfo.End = this.StartOn.AddMinutes(this.Course.LessonDuration);
                        app.RecurrenceInfo.Type = DevExpress.XtraScheduler.RecurrenceType.Weekly;
                        //app.RecurrenceInfo.Duration = new TimeSpan(0, this.Course.LessonDuration, 0);
                        app.RecurrenceInfo.OccurrenceCount = this.Course.LessonCount;
                        app.RecurrenceInfo.Range = DevExpress.XtraScheduler.RecurrenceRange.OccurrenceCount;
                        AppointmentRecurrenceForm form = new AppointmentRecurrenceForm(app, FirstDayOfWeek.Monday, null);
                        DialogResult result = form.ShowDialog();

                        if (result == DialogResult.OK)
                        {

                            RecurrenceInfoXmlPersistenceHelper helper =
                                new RecurrenceInfoXmlPersistenceHelper(app.RecurrenceInfo, DateSavingType.LocalTime);
                            //Creating lesson 
                            lsn = new Lesson(this.Session)
                            {
                                StartOn = app.Start,
                                EndOn = app.End,
                                Type = (int)AppointmentType.Pattern,
                                RecurrenceInfoXml = helper.ToXml(),
                                Group = this,
                                Tutor = this.Tutor,
                                Subject = this.Course.Name,
                                Location = this.Location.Title


                            };
                            foreach (GroupStudentDetail item in StudentDetails)
                            {
                                lsn.StudentDetails.Add(new LessonStudentDetail(Session) { Student = item.Student, Status = LessonStudentStatus.none });
                                lsn.Students.Add(item.Student);
                            }
                            Lessons.Add(lsn);
                            this.Session.Save(lsn);
                            Session.Save(this);

                        }
                        else
                        {
                            isCreating = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    isCreating = false;
                    WinApplication.Messaging.Show("Exception message", ex.Message);
                }
                finally
                {
                    if (isCreating)
                    {
                        WinApplication.Messaging.Show("Information", "Lessons are generated");
                    }
                    else
                    {
                        WinApplication.Messaging.Show("Information", "Generation canceled");
                    }

                }
            }
            else
            {
                WinApplication.Messaging.Show("Information", "Please add student at first, then try to generate invoices");
            }



        }

        [Action]
        public void GenerateInvoices()
        {
            if (StudentDetails.Count > 0)
            {
                //Add invoice for Tutor
                if (Tutor != null)
                {
                    TutorInvoice TutorInvoice = new TutorInvoice(Session)
                    {
                        InvoiceDate = this.StartOn,
                        InvoiceNumber = String.Format("INV-{0}-{1}", this.Course.Name, DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty)),
                    };
                    TutorInvoice.InvoiceItems.Add(new InvoiceItem(Session)
                    {
                        Amount = TutorFee,
                        Name = String.Format("For {0} Lessons", this.Course.Name),
                    });
                    Tutor.Invoices.Add(TutorInvoice);
                }
                //Add invoice for Students
                foreach (GroupStudentDetail Sdetail in StudentDetails)
                {
                    StudentInvoice NewStudentInvoice =
                        new StudentInvoice(Session)
                        {
                            Group = this,
                            InvoiceDate = this.StartOn,
                            InvoiceNumber =
                            String.Format("INV-{0}-{1}", this.Course.Name, DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty)),
                            //TotalCost=this.Course.Cost                            
                        };
                    //This code moved to AfterConstruction
                    NewStudentInvoice.InvoiceItems.Add(new InvoiceItem(Session)
                                                    {
                                                        Name = String.Format("For {0} Lessons", this.Course.Name),
                                                        Amount =  Sdetail.Amount
                                                    });

                    Sdetail.Student.StudentInvoices.Add(NewStudentInvoice);
                    WinApplication.Messaging.Show("Information", "Invoices are generated!");
                }
            }
            else
            {
                WinApplication.Messaging.Show("Information", "Please add student at first, then try to generate invoices");
            }
        }

        #region Association

        [Association("Group-StudentDetails", typeof(GroupStudentDetail)), Aggregated]
        public XPCollection<GroupStudentDetail> StudentDetails
        {
            get { return GetCollection<GroupStudentDetail>("StudentDetails"); }
        }


        [Association("Group-Lessons", typeof(Lesson))]
        public XPCollection<Lesson> Lessons
        {
            get { return GetCollection<Lesson>("Lessons"); }
        }

        protected Tutor _tutor;
        [Association("Groupes-Tutor", typeof(Tutor))]
        public Tutor Tutor
        {
            get { return _tutor; }
            set { SetPropertyValue<Tutor>("Tutor", ref _tutor, value); }
        }
        #endregion
    }
}