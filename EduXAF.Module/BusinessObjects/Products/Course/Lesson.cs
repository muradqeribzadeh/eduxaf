using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using EduXAF.Module.BusinessObjects.CRM.Student;
using DevExpress.Xpo.Metadata;
using System.Xml;
using DevExpress.ExpressApp.Model;
using EduXAF.Module.BusinessObjects.HR.Tutor;

namespace EduXAF.Module.BusinessObjects.Products.Course
{
    [DefaultClassOptions]
    public class Lesson : BaseObject, IEvent, IRecurrentEvent
    {
        private bool _AllDay;
        private string _Description;
        private DateTime _StartOn;
        private DateTime _EndOn;
        private int _Label;
        private string _Location;
        private int _Status;
        private string _Subject;
        private int _Type;
        private string _RecurrenceInfoXml;
        [Persistent("ResourceIds"), Size(SizeAttribute.Unlimited)]
        private string _StudentIds;
        [Persistent("RecurrencePattern")]
        private Lesson _RecurrencePattern;
        public Lesson(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            StartOn = DateTime.Now;
            EndOn = StartOn.AddHours(1);
            Students.Add(Session.GetObjectByKey<Student>(SecuritySystem.CurrentUserId));
        }

        [Association("Lesson-Students", typeof(Student))]
        public XPCollection<Student> Students
        {
            get { return GetCollection<Student>("Students"); }
        }

        [Association("Lesson-StudentDetails", typeof(LessonStudentDetail))]
        public XPCollection<LessonStudentDetail> StudentDetails
        {
            get { return GetCollection<LessonStudentDetail>("StudentDetails"); }
        }

        protected override XPCollection<T> CreateCollection<T>(XPMemberInfo property)
        {
            XPCollection<T> result = base.CreateCollection<T>(property);
            if (property.Name == "Students")
            {
                result.ListChanged += Students_ListChanged;
            }
            return result;
        }
        public void UpdateStudentIds()
        {
            _StudentIds = string.Empty;
            if (StudentDetails != null)
                foreach (Student activityUser in Students)
                {
                    _StudentIds += String.Format(@"<ResourceId Type=""{0}"" Value=""{1}"" />", activityUser.Id.GetType().FullName, activityUser.Id);
                }
            if (Tutor != null)
            {
                _StudentIds += string.Format("<ResourceId Type=\"{0}\" Value=\"{1}\" />\r\n",
                                             Tutor.Id.GetType().FullName, Tutor.Id);
            }
            if (Location != null)
            {
                _StudentIds += string.Format("<ResourceId Type=\"{0}\" Value=\"{1}\" />\r\n",
                                             Location.GetType().FullName, Location);
            }
            _StudentIds = String.Format("<ResourceIds>{0}</ResourceIds>", _StudentIds);
        }
        private void UpdateStudents()
        {
            
            Students.SuspendChangedEvents();
            try
            {
                while (Students.Count > 0)
                    Students.Remove(Students[0]);
                if (!String.IsNullOrEmpty(_StudentIds))
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(_StudentIds);
                    Student activityUser;
                    Tutor lessonTutor;

                    foreach (XmlNode xmlNode in xmlDocument.DocumentElement.ChildNodes)
                    {
                        Guid Xoid=Guid.Empty;
                        try
                        {
                            Xoid = new Guid(xmlNode.Attributes["Value"].Value);
                            activityUser = Session.GetObjectByKey<Student>(Xoid);

                        }
                        catch { activityUser = null; }

                        try
                        {
                            Xoid = new Guid(xmlNode.Attributes["Value"].Value);
                            lessonTutor = Session.GetObjectByKey<Tutor>(Xoid);
                        }
                        catch { lessonTutor = null; }



                        if (activityUser != null)
                        {
                            Students.Add(activityUser);
                            
                            StudentDetails.Add(new LessonStudentDetail(Session)
                            {
                                Status = LessonStudentStatus.none,
                                Student = activityUser
                            });
                            
                        }

                        if (lessonTutor != null)
                        {
                            Tutor = lessonTutor;
                        }

                    }
                }
            }
            finally
            {
                Students.ResumeChangedEvents();
            }
        }
        public void UpdateStudentDetails()
        {
            //foreach (LessonStudentDetail student in StudentDetails)
            //{

            //}
        }

        private void Students_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)// || e.ListChangedType == ListChangedType.ItemDeleted)
            {
                UpdateStudentIds();
                OnChanged("ResourceId");
            }
        }
        protected override void OnLoaded()
        {
            base.OnLoaded();
            if (Students.IsLoaded && !Session.IsNewObject(this))
                Students.Reload();
        }
        [NonPersistent]
        [Browsable(false)]
        [RuleFromBoolProperty("EventIntervalValid", DefaultContexts.Save, "The start date must be less than the end date", SkipNullOrEmptyValues = false, UsedProperties = "StartOn, EndOn")]
        public bool IsIntervalValid { get { return StartOn <= EndOn; } }
        #region IEvent Members
        public bool AllDay
        {
            get { return _AllDay; }
            set { SetPropertyValue("AllDay", ref _AllDay, value); }
        }
        [Browsable(false), NonPersistent]
        public object AppointmentId
        {
            get { return Oid; }
        }
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }
        public int Label
        {
            get { return _Label; }
            set { SetPropertyValue("Label", ref _Label, value); }
        }
        public string Location
        {
            get { return _Location; }
            set { SetPropertyValue("Location", ref _Location, value); }
        }
        [PersistentAlias("_StudentIds"), Browsable(false)]
        public string ResourceId
        {
            get
            {
                if (_StudentIds == null)
                    UpdateStudentIds();
                return _StudentIds;
            }
            set
            {
                if (_StudentIds != value && value != null)
                {
                    _StudentIds = value;
                    UpdateStudents();
                }
            }
        }
        [Indexed]
        [ModelDefault("DisplayFormat", "{0:G}")]
        [ModelDefault("EditMask", "G")]
        public DateTime StartOn
        {
            get { return _StartOn; }
            set { SetPropertyValue("StartOn", ref _StartOn, value); }
        }
        [Indexed]
        [ModelDefault("DisplayFormat", "{0:G}")]
        [ModelDefault("EditMask", "G")]
        public DateTime EndOn
        {
            get { return _EndOn; }
            set { SetPropertyValue("EndOn", ref _EndOn, value); }
        }
        public int Status
        {
            get { return _Status; }
            set { SetPropertyValue("Status", ref _Status, value); }
        }
        [Size(250)]
        public string Subject
        {
            get { return _Subject; }
            set { SetPropertyValue("Subject", ref _Subject, value); }
        }
        [Browsable(false)]
        public int Type
        {
            get { return _Type; }
            set { SetPropertyValue("Type", ref _Type, value); }
        }
        #endregion

        #region IRecurrentEvent Members
        [DevExpress.Xpo.DisplayName("Recurrence"), Size(SizeAttribute.Unlimited)]
        public string RecurrenceInfoXml
        {
            get { return _RecurrenceInfoXml; }
            set { SetPropertyValue("RecurrenceInfoXml", ref _RecurrenceInfoXml, value); }
        }
        [Browsable(false)]
        [PersistentAlias("_RecurrencePattern")]
        public IRecurrentEvent RecurrencePattern
        {
            get { return _RecurrencePattern; }
            set { SetPropertyValue("RecurrencePattern", ref _RecurrencePattern, value as Lesson); }
        }
        #endregion

        #region Memmbers

        private Tutor _tutor;
        public Tutor Tutor
        {
            get
            {
                return _tutor;
            }
            set { SetPropertyValue<Tutor>("Tutor", ref _tutor, value); }
        }

        private Group _Group;
        [Association("Group-Lessons", typeof(Group))]
        public Group Group
        {
            get { return _Group; }
            set
            {
                SetPropertyValue<Group>("Group", ref _Group, value);
            }
        }
        #endregion
    }
}
