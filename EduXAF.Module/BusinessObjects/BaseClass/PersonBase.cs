using System;
using System.ComponentModel;
using System.Drawing;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Filtering;
using DevExpress.Xpo.Metadata;
using EduXAF.Module.BusinessObjects.CRM;

namespace EduXAF.Module.BusinessObjects.BaseClass
{
    [DefaultClassOptions]
    public class PersonBase : BaseObject
    {

        #region Constructions
        public PersonBase(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            //// if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        #endregion

        #region Properties

        private string _Email;
        private Address _PostalAddress;
        private Address _HomeAddress;
        private DateTime _Birthday;
        private string _firstname;
        private string _lastname;
        private string _patronymic;
        private TitleOfCourtesy titleOfCourtesy;
        private Image _Photo;
        private Gender _Gender;
        private Country _BirthCountry;

        //public TitleOfCourtesy TitleOfCourtesy
        //{
        //    get { return titleOfCourtesy; }
        //    set { SetPropertyValue("TitleOfCourtesy", ref titleOfCourtesy, value); }
        //}
        public string FirstName
        {
            get { return _firstname; }
            set { SetPropertyValue("FirstName", ref _firstname, value); }
        }


        public string LastName
        {
            get { return _lastname; }
            set { SetPropertyValue("LastName", ref _lastname, value); }
        }

        public string Patronymic
        {
            get { return _patronymic; }
            set { SetPropertyValue("Patronymic", ref _patronymic, value); }
        }

     
        public string FullName { get { return String.Format("{0} {1}", LastName, FirstName); } }


        public string Email
        {
            get { return _Email; }
            set { SetPropertyValue("Email", ref _Email, value); }
        }


        public Gender Gender
        {
            get { return _Gender; }
            set { SetPropertyValue("Patronymic", ref _Gender, value); }

        }

        public DateTime Birthday
        {
            get { return _Birthday; }
            set { SetPropertyValue("Birthday", ref _Birthday, value); }
        }

        public Country BirthCountry
        {
            get { return _BirthCountry; }
            set { SetPropertyValue("BirthCountry", ref _BirthCountry, value); }
        }

        [ValueConverter(typeof(ImageValueConverter))]
        //[Delayed(true)]
        //[Size(-1)]
        public Image Photo
        {
            get { return _Photo; }
            set { SetPropertyValue("Photo", ref _Photo, value); }
        }

        [Aggregated]
        [ExpandObjectMembers(ExpandObjectMembers.Never)]
        public Address HomeAddress
        {
            get { return _HomeAddress; }
            set { SetPropertyValue("HomeAddress", ref _HomeAddress, value); }
        }

        //[Aggregated]
        //[ExpandObjectMembers(ExpandObjectMembers.Never)]
        //public Address PostalAddress
        //{
        //    get { return _PostalAddress; }
        //    set { SetPropertyValue("PostalAddress", ref _PostalAddress, value); }
        //}
        #endregion

        #region Associations

        [Association("PersonBase-PhoneContact", typeof(PhoneContact)), Aggregated]
        public XPCollection<PhoneContact> PhoneContacts
        {
            get
            {
                return GetCollection<PhoneContact>("PhoneContacts");
            }
        }

        
        #endregion
    }

}
