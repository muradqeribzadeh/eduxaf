﻿using DevExpress.ExpressApp;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EduXAF.Module.BusinessObjects.HR;
using EduXAF.Module.BusinessObjects.OfficeBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduXAF.Module.BusinessObjects.CashCore
{
    [DefaultClassOptions]
    public class CashBase : BaseObject
    { // You can use a different base persistent class based on your requirements (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public CashBase(Session session)
            : base(session)
        {
       
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            InputDate = DateTime.Now;
            EduUser CurrentUser = Session.GetObjectByKey<EduUser>((SecuritySystem.CurrentUser as EduUser).Oid);
            if (CurrentUser.DefaultOffice != null)
                Office = CurrentUser.DefaultOffice;
            if (CurrentUser.DefaultCash != null)
                Cash = CurrentUser.DefaultCash;
            Author = CurrentUser;
            //InvoiceNumber = "K-INV-" + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);
        }

        // Fields...
        private string _Comment;
        private decimal _Amount;
        private DateTime _InputDate;
        private Cash _Cash;
        private Office _Office;
        private EduUser _Author;



        [RuleRequiredField(DefaultContexts.Save)]
        public EduUser Author
        {
            get
            {
                return _Author;
            }
            set
            {
                SetPropertyValue("Author", ref _Author, value);
            }
        }
        [RuleRequiredField(DefaultContexts.Save)]
        public Cash Cash
        {
            get
            {
                return _Cash;
            }
            set
            {
                SetPropertyValue("Cash", ref _Cash, value);
            }
        }

        public DateTime InputDate
        {
            get
            {
                return _InputDate;
            }
            set
            {
                SetPropertyValue("InputDate", ref _InputDate, value);
            }
        }


        public decimal Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                SetPropertyValue("Amount", ref _Amount, value);
            }
        }



        [Size(500)]
        public string Comment
        {
            get
            {
                return _Comment;
            }
            set
            {
                SetPropertyValue("Comment", ref _Comment, value);
            }
        }
        [RuleRequiredField(DefaultContexts.Save)]
        public Office Office
        {
            get
            {
                return _Office;
            }
            set
            {
                SetPropertyValue("Office", ref _Office, value);
            }
        }
    }
}
