﻿using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using EduXAF.Module.BusinessObjects.HR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduXAF.Module.BusinessObjects.CashCore
{
    [DefaultClassOptions]
    public class Cash : BaseObject
    {
        public Cash(Session session)
            : base(session)
        {

        }
        // Fields...
        private string _Name;
        private HRBase _Cashier;

        public HRBase Cashier
        {
            get
            {
                return _Cashier;
            }
            set
            {
                SetPropertyValue("Cashier", ref _Cashier, value);
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }
        protected override void OnLoaded()
        {
            _totalAmount = null;
            base.OnLoaded();
        }
        [Association("Cash-CashHistories"), DevExpress.Xpo.Aggregated]
        public XPCollection<CashHistory> CashHistories
        {
            get
            {

                return GetCollection<CashHistory>("CashHistories");
            }
        }

        [Persistent("TotalAmount")]
        private decimal? _totalAmount = null;
        [PersistentAlias("_totalAmount")]
        public decimal? TotalAmount
        {
            get
            {
                if (!IsLoading && !IsSaving && _totalAmount == null)
                    UpdateTotalAmount(false);
                return _totalAmount;
            }
        }

        public void UpdateTotalAmount(bool forceChangeEvents)
        {
            decimal? oldOrdersTotal = _totalAmount;
            decimal tempTotal = 0m;
            foreach (CashHistory detail in CashHistories)
                tempTotal += detail.Amount;
            _totalAmount = tempTotal;
            if (forceChangeEvents)
                OnChanged("TotalAmount", oldOrdersTotal, _totalAmount);

        }

    }
}
