﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EduXAF.Module.BusinessObjects.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace EduXAF.Module.BusinessObjects.CashCore
{
    [DefaultClassOptions]
    public class ExpenseDocument : CashBase
    { // You can use a different base persistent class based on your requirements (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public ExpenseDocument(Session session)
            : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            InvoiceNumber = "XQ-" + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);


        }
        protected override void OnDeleted()
        {
            DoDelete(Cash, Oid);
        }
        void DoDelete(Cash Oldcash, Guid Did)
        {
            using (UnitOfWork uow = new UnitOfWork(Session.DataLayer))
            {
                //Delete Cash History
                Cash cash = uow.GetObjectByKey<Cash>(Oldcash.Oid);
                IEnumerable<CashHistory> CashCollection = cash.CashHistories.Where(h => h.ResourceID == Did);
                foreach (CashHistory history in CashCollection.ToList())
                {
                    uow.GetObjectByKey<CashHistory>(history.Oid).Delete();
                }
                cash.Save();

                uow.CommitChanges();
            }
        }
        // Fields...

        private string _invoiceNumber;
        [ModelDefault("AllowEdit", "False")]
        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { SetPropertyValue<string>("InvoiceNumber", ref _invoiceNumber, value); }
        }

        [Browsable(false)]
        public decimal Amount
        {
            get;
            set;
        }

        [Persistent("TotalAmount")]
        private decimal? _totalAmount = null;
        [PersistentAlias("_totalAmount")]
        public decimal? TotalAmount
        {
            get
            {
                if (!IsLoading && !IsSaving && _totalAmount == null)
                    UpdateTotalAmount(false);
                return _totalAmount;
            }
        }

        public void UpdateTotalAmount(bool forceChangeEvents)
        {
            decimal? oldOrdersTotal = _totalAmount;
            decimal tempTotal = 0m;
            foreach (ExpenceDetails detail in Details)
                tempTotal += detail.Amount;
            _totalAmount = tempTotal;
            if (forceChangeEvents)
                OnChanged("TotalAmount", oldOrdersTotal, _totalAmount);

        }
        [Association("ExpenseDocument-ExpenceDetails"), Aggregated]
        public XPCollection<ExpenceDetails> Details
        {
            get
            {
                return GetCollection<ExpenceDetails>("Details");
            }
        }

    }
    [DefaultClassOptions, DefaultProperty("Worker")]
    public class ExpenceDetails : BaseObject
    {
        public ExpenceDetails(Session session)
            : base(session)
        { }

        public override void AfterConstruction()
        {
            EduUser CurrentUser = Session.GetObjectByKey<EduUser>((SecuritySystem.CurrentUser as EduUser).Oid);
            if (CurrentUser.HRUser != null)
                HRUser = CurrentUser.HRUser;
            Quantity = 1;
            base.AfterConstruction();
        }

        // Fields...
        private Expence _Expense;
        private ExpenseDocument _RootDocument;
        private string _Description;
        private int _Quantity;
        private HRBase _HRUser;
        [RuleRequiredField(DefaultContexts.Save)]
        public Expence Expense
        {
            get
            {
                return _Expense;
            }
            set
            {
                SetPropertyValue("Expense", ref _Expense, value);
            }
        }
       
     

        private decimal fTotal;
        [RuleRange(DefaultContexts.Save, 0, int.MaxValue)]
        public decimal Amount
        {
            get { return fTotal; }
            set
            {
                SetPropertyValue("Amount", ref fTotal, value);
                if (!IsLoading && !IsSaving && RootDocument != null)
                {
                    RootDocument.UpdateTotalAmount(true);
                }
            }
        }
         [RuleRequiredField(DefaultContexts.Save)]
        public HRBase HRUser
        {
            get
            {
                return _HRUser;
            }
            set
            {
                SetPropertyValue("HRUser", ref _HRUser, value);
            }
        }
        [RuleRange(DefaultContexts.Save, 1, int.MaxValue)]
        public int Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                SetPropertyValue("Quantity", ref _Quantity, value);
            }
        }

        public decimal Cost
        {
            get
            {
                if (Amount > 0M && Quantity > 0)
                    return Amount / (decimal)Quantity;
                else
                    return 0;
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                SetPropertyValue("Description", ref _Description, value);
            }
        }

        [Association("ExpenseDocument-ExpenceDetails")]
        public ExpenseDocument RootDocument
        {
            get
            {
                return _RootDocument;
            }
            set
            {
                ExpenseDocument oldDocument = _RootDocument;
                SetPropertyValue("RootDocument", ref _RootDocument, value);
                if (!IsLoading && !IsSaving && oldDocument != _RootDocument)
                {
                    oldDocument = oldDocument ?? _RootDocument;
                    oldDocument.UpdateTotalAmount(true);
                }
            }
        }
    

    }
}
