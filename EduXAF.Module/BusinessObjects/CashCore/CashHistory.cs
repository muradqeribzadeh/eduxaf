﻿using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduXAF.Module.BusinessObjects.CashCore
{
    [DefaultClassOptions]
    public class CashHistory : BaseObject
    {
        public CashHistory(Session session)
            : base(session)
        {

        }
        private Cash _Cash;
        private string _RefferencedInvoiceNumber;
        private Guid _ResourceID;
        private CashHistoryType _Type;
        private string _Status;
        private DateTime _InputDate;
        private decimal _Amount;

        public decimal Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                SetPropertyValue("Amount", ref _Amount, value);
                if (!IsLoading && !IsSaving && Cash != null)
                {
                    Cash.UpdateTotalAmount(true);
                }
            }
        }
        public DateTime InputDate
        {
            get
            {
                return _InputDate;
            }
            set
            {
                SetPropertyValue("InputDate", ref _InputDate, value);
            }
        }
        public string RefferencedInvoiceNumber
        {
            get
            {
                return _RefferencedInvoiceNumber;
            }
            set
            {
                SetPropertyValue("RefferencedInvoiceNumber", ref _RefferencedInvoiceNumber, value);
            }
        }
        public Guid ResourceID
        {
            get
            {
                return _ResourceID;
            }
            set
            {
                SetPropertyValue("ResourceID", ref _ResourceID, value);
            }
        }
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                SetPropertyValue("Status", ref _Status, value);
            }
        }
        public CashHistoryType Type
        {
            get
            {
                return _Type;
            }
            set
            {
                SetPropertyValue("Type", ref _Type, value);
            }
        }

        [Association("Cash-CashHistories")]
        public Cash Cash
        {
            get
            {
                return _Cash;
            }
            set
            {
                SetPropertyValue("Cash", ref _Cash, value);
            }
        }



    }


}
