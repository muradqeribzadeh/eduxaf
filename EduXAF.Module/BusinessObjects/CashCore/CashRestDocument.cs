﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EduXAF.Module.BusinessObjects.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace EduXAF.Module.BusinessObjects.CashCore
{
    [DefaultClassOptions]
    public class CashRestDocument : CashBase
    { // You can use a different base persistent class based on your requirements (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public CashRestDocument(Session session)
            : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            InvoiceNumber = "KQS-" + DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty);


        }
        protected override void OnDeleted()
        {
            DoDelete(Cash, Oid);
        }
        void DoDelete(Cash Oldcash, Guid Did)
        {
            using (UnitOfWork uow = new UnitOfWork(Session.DataLayer))
            {
                //Delete Cash History
                Cash cash = uow.GetObjectByKey<Cash>(Oldcash.Oid);
                IEnumerable<CashHistory> CashCollection = cash.CashHistories.Where(h => h.ResourceID == Did);
                foreach (CashHistory history in CashCollection.ToList())
                {
                    uow.GetObjectByKey<CashHistory>(history.Oid).Delete();
                }
                cash.Save();

                uow.CommitChanges();
            }
        }
        // Fields...
        private string _invoiceNumber;
        [ModelDefault("AllowEdit", "False")]
        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { SetPropertyValue<string>("InvoiceNumber", ref _invoiceNumber, value); }
        }
     
    }
}
