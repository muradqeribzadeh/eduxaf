using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using EduXAF.Module.BusinessObjects.BaseClass;


namespace EduXAF.Module.BusinessObjects.HR
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    public class HRBase : PersonBase
    {
        private HRBase manager;
        private string passportNumber;
        private DateTime dateInPosition;
        private float experienceInYears;
        private string notes;

        public HRBase(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }


        [DataSourceProperty("Department.Workers", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Position.Title = 'Manager'")]
        public HRBase Manager
        {
            get
            {
                return manager;
            }
            set
            {
                SetPropertyValue("Manager", ref manager, value);
            }
        }
        private Department department;

        [Association("Department-Workers", typeof(Department)), ImmediatePostData]
        public Department Department
        {
            get
            {
                return department;
            }
            set
            {
                SetPropertyValue(propertyName: "Department", propertyValueHolder: ref department, newValue: value);
                if (!IsLoading)
                {
                    Position = null;
                    if (Manager != null && Manager.Department != value)
                    {
                        Manager = null;
                    }
                }
            }
        }

        private Position position;

        public Position Position
        {
            get
            {
                return position;
            }
            set
            {
                SetPropertyValue("Position", ref position, value);
            }
        }

        public DateTime DateInPosition
        {
            get { return dateInPosition; }
            set { dateInPosition = value; }
        }

        public float ExperienceInYears
        {
            get { return experienceInYears; }
            set { experienceInYears = value; }
        }


        [Size(4096)]
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
    }

}
