using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using EduXAF.Module.BusinessObjects.OfficeBase;

namespace EduXAF.Module.BusinessObjects.HR
{
    [DefaultClassOptions]
    [System.ComponentModel.DefaultProperty("Title")]
    public class Department : BaseObject
    {
        private Office office ;
        private string title;
        public Department(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                SetPropertyValue("Title", ref title, value);
            }
        }

        public Office Office
        {
            get
            {
                return office;
            }
            set
            {
                SetPropertyValue("Office", ref office, value);
            }
        }

        [Association("Department-Workers", typeof(HRBase))]
        public XPCollection Workers
        {
            get
            {
                return GetCollection("Workers");
            }
        }

        [Association("Departments-Positions", typeof(Position))]
        public XPCollection Positions
        {
            get
            {
                return GetCollection("Positions");
            }
        }

    }

}
