﻿using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using EduXAF.Module.BusinessObjects.CashCore;
using EduXAF.Module.BusinessObjects.OfficeBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduXAF.Module.BusinessObjects.HR
{
    [DefaultClassOptions]
    public class EduUser : SecuritySystemUser
    { // You can use a different base persistent class based on your requirements (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public EduUser(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code (check out http://documentation.devexpress.com/#Xaf/CustomDocument2834 for more details).
        }
        private Cash _DefaultCash;
        private Office _DefaultOffice;
        private HRBase _HRUser;
        public Cash DefaultCash
        {
            get
            {
                return _DefaultCash;
            }
            set
            {
                SetPropertyValue("DefaultCash", ref _DefaultCash, value);
            }
        }
        public Office DefaultOffice
        {
            get
            {
                return _DefaultOffice;
            }
            set
            {
                SetPropertyValue("DefaultOffice", ref _DefaultOffice, value);
            }
        }
        public HRBase HRUser
        {
            get
            {
                return _HRUser;
            }
            set
            {
                SetPropertyValue("HRUser", ref _HRUser, value);
            }
        }
        
    }
}
