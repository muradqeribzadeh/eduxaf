using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using System.Drawing;
using EduXAF.Module.BusinessObjects.Payment;
using EduXAF.Module.BusinessObjects.Products.Course;

namespace EduXAF.Module.BusinessObjects.HR.Tutor
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [ImageName("BO_Employee"), System.ComponentModel.DisplayName("Tutor")]
    public class Tutor : HRBase, IResource
    {
        private string passportNumber;
        public Tutor(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here or place it only when the IsLoading property is false:
            // if (!IsLoading){
            //    It is now OK to place your initialization code here.
            // }
            // or as an alternative, move your initialization code into the AfterConstruction method.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.

            _color = Color.White.ToArgb();
        }

        public string PassportNumber
        {
            get { return passportNumber; }
            set { passportNumber = value; }
        }

       
        [Association("Tutor-Invoices", typeof(TutorInvoice)), Aggregated]
        public XPCollection<TutorInvoice> Invoices
        {
            get { return GetCollection<TutorInvoice>("Invoices"); }
        }
        #region IResource Members

#if MediumTrust
		[Persistent("Color")]
		[Browsable(false)]
		public Int32 color;
#else
        [Persistent("Color")]
        private Int32 _color;
#endif

        [NonPersistent, Browsable(false)]
        public object Id
        {
            get { return Oid; }
        }

        [NonPersistent, Browsable(false)]
        public string Caption
        {
            get { return FullName; }
            set
            {
                //  _caption = value;
                //  OnChanged("Caption");
            }
        }

        [NonPersistent, Browsable(false)]
        public Int32 OleColor
        {
            get { return ColorTranslator.ToOle(Color.FromArgb(_color)); }
        }

        [Association("Groupes-Tutor", typeof(Group))]
        public XPCollection Groups
        {
            get { return GetCollection("Groups"); }
        }

        [NonPersistent, Browsable(false)]
        public Color Color
        {
            get { return Color.FromArgb(_color); }
            set
            {
                _color = value.ToArgb();
                OnChanged("Color");
            }
        }

        #endregion IResource Members
    }

}
