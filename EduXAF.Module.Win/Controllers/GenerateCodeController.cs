using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using EduXAF.Module.BusinessObjects.CRM.Student;
using System.Windows.Forms;

namespace EduXAF.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class GenerateCodeController : ViewController
    {
        public GenerateCodeController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(Student);
            TargetViewType = ViewType.DetailView;
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

        }
        protected override void OnViewControlsCreated()
        {
            DetailView view = (DetailView)View;
            foreach (ControlDetailItem item in view.GetItems<ControlDetailItem>())
            {
                if (item.Id == "btnBarcode")
                {
                    (item.Control as Button).Text = "New card number";
                    (item.Control as Button).Click += ButtonClickHandlingWinController_Click;
                    break;
                }
            }
        }

        void ButtonClickHandlingWinController_Click(object sender, EventArgs e)
        {
            Student PU = View.CurrentObject as Student;
            string Barcode = "";
            while (CheckCountBarcodeAvability(Barcode) == 1)
            {
                Barcode = GenerateNewBarcode();
            }
            PU.CardNumber = Barcode;
        }
        public string GenerateNewBarcode()
        {
            const string firstNumber = "220";
            Random rnd = new Random();
            int ten = rnd.Next(100000000, 999999999);
            string sTemp = firstNumber + ten.ToString();

            //Calculate Checksum
            int iSum = 0;
            int iDigit = 0;

            // Calculate the checksum digit here.
            for (int i = sTemp.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));
                if (i % 2 == 0)
                {	// odd
                    iSum += iDigit * 3;
                }
                else
                {	// even
                    iSum += iDigit * 1;
                }
            }

            int iCheckSum = (10 - (iSum % 10)) % 10;
            return sTemp + iCheckSum.ToString();
        }

        public int CheckCountBarcodeAvability(string barcode)
        {
            if (string.IsNullOrEmpty(barcode))
                return 1;
            var obj = ObjectSpace.FindObject<Student>(CriteriaOperator.Parse("CardNumber=?", barcode));
            if (obj == null)
                return 0;
            else
                return 1;
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
