namespace EduXAF.Module.Win.Controllers
{
    partial class FilterTutorsController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tutorsChoiseAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // tutorsChoiseAction
            // 
            this.tutorsChoiseAction.Caption = "Tutors";
            this.tutorsChoiseAction.Category = "View";
            this.tutorsChoiseAction.ConfirmationMessage = null;
            this.tutorsChoiseAction.Id = "TutorssChoiseAction";
            this.tutorsChoiseAction.ImageName = "BO_Image";
            this.tutorsChoiseAction.Shortcut = null;
            this.tutorsChoiseAction.Tag = null;
            this.tutorsChoiseAction.TargetObjectsCriteria = null;
            this.tutorsChoiseAction.TargetViewId = null;
            this.tutorsChoiseAction.ToolTip = null;
            this.tutorsChoiseAction.TypeOfView = null;
            this.tutorsChoiseAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.tutorsChoiseAction_Execute_1);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction tutorsChoiseAction;
    }
}
