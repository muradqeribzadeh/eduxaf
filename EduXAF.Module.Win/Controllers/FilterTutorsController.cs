using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using EduXAF.Module.BusinessObjects.CRM.Student;
using System.Windows.Forms;
using DevExpress.ExpressApp.Scheduler.Win;
using DevExpress.Xpo;
using EduXAF.Module.BusinessObjects.Products.Course;
using EduXAF.Module.BusinessObjects.HR.Tutor;

namespace EduXAF.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class FilterTutorsController : ViewController
    {
      //  SingleChoiceAction studentChoiseAction;


        public FilterTutorsController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(Lesson);
            TargetViewId = "Lesson_ListView_Tutor";
            //studentChoiseAction = new SingleChoiceAction();
            //studentChoiseAction.Id = "StudentChoiseAction";
            //studentChoiseAction.ImageName = "BO_Person";
            //studentChoiseAction.Caption = "Students";
            //studentChoiseAction.Category = PredefinedCategory.View.ToString();

            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            tutorsChoiseAction.Items.Clear();

            ChoiceActionItem allUsersActionItem = new ChoiceActionItem();
            allUsersActionItem.Caption = "All Tutors";
            tutorsChoiseAction.Items.Add(allUsersActionItem);

            foreach (Tutor tutor in View.ObjectSpace.GetObjects<Tutor>())
            {
                tutorsChoiseAction.Items.Add(new ChoiceActionItem(tutor.FullName, tutor.Oid));
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            SchedulerListEditor editor = ((DevExpress.ExpressApp.ListView)View).Editor as SchedulerListEditor;
            if (editor == null)
            {
                tutorsChoiseAction.Active.SetItemValue("Scheduler", false);
            }
            else
            {
                tutorsChoiseAction.Active.SetItemValue("Scheduler", true);
                tutorsChoiseAction.SelectedItem = tutorsChoiseAction.Items[0];
                editor.ResourceDataSourceCreated += editor_ResourceDataSourceCreated;
            }
        }

        void editor_ResourceDataSourceCreated(object sender, DevExpress.ExpressApp.Scheduler.ResourceDataSourceCreatedEventArgs e)
        {
            SetResourcesFilter(e.DataSource, tutorsChoiseAction.SelectedItem.Data);
       
        }
        private void SetResourcesFilter(Object dataSource, Object resourceId)
        {
            XPCollection resources = dataSource as XPCollection;
            if (resourceId == null)
            {
                resources.Criteria = null;
            }
            else
            {
                resources.Criteria = new BinaryOperator("Oid", resourceId);
            }
        }
 

        private void tutorsChoiseAction_Execute_1(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            SchedulerListEditor editor = ((DevExpress.ExpressApp.ListView)View).Editor as SchedulerListEditor;
            SetResourcesFilter(editor.ResourcesDataSource, e.SelectedChoiceActionItem.Data);
        }
         
    }
}
