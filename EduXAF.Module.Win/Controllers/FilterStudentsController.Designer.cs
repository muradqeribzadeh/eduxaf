namespace EduXAF.Module.Win.Controllers
{
    partial class FilterStudentsController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.studentsChoiseAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // studentsChoiseAction
            // 
            this.studentsChoiseAction.Caption = "Students";
            this.studentsChoiseAction.Category = "View";
            this.studentsChoiseAction.ConfirmationMessage = null;
            this.studentsChoiseAction.Id = "StudentsChoiseAction";
            this.studentsChoiseAction.ImageName = null;
            this.studentsChoiseAction.Shortcut = null;
            this.studentsChoiseAction.Tag = null;
            this.studentsChoiseAction.TargetObjectsCriteria = null;
            this.studentsChoiseAction.TargetViewId = null;
            this.studentsChoiseAction.ToolTip = null;
            this.studentsChoiseAction.TypeOfView = null;
            this.studentsChoiseAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.studentsChoiseAction_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction studentsChoiseAction;
    }
}
